# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: сменить контекст вызова можно через bind, call, apply.
> bind используется, когда необходимо вызвать функцию с определённым контектом, но не в данный момент, а в целом когда-то. При этом параменты функции можно не передавать, а можно передать какую-то часть при bind, а затем остальные при вызове функции. 
> call - в отличие от bind, меняет контекст непосредственно при вызове фукции. При этом параметры для вызова функции (если они необходимы) нужно передавть тут же в call, перечислив их по одному через запятую.
> apply - также как и call вызывает функцию с указанным контестом, но парметры передаются не по одному, а узказанные в массиве (массивоподобном объекте).
#### 2. Что такое стрелочная функция?
> Ответ: Это по сути более короткий вариант function expression, но с дополнительными особенностями.
```js 
let sum = (a, b) => a + b;
// Более короткая запись для:
let sum = function(a, b) {
  return a + b;
};
```
>Такие функции не имют своего контекста вызова и захватывают this снаружи. Поэтому они также не могут быть использованы как конструкторы.
#### 3. Приведите свой пример конструктора. 
```js
function Student(name, specialty) {
  this.name = name;
  this.specialty = specialty;
  this.isHungry = true;
}

let Dasha = new Student('Dasha', 'programmer')
```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
  //исходный код
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();

  //первый способ
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(() => console.log(this.name + ' says hello to everyone!'), 1000);
    }
  }

  person.sayHello();

  //второй способ
  const person = {
    name: 'Nikita',
    sayHello: function() {
      const that = this;

      setTimeout(function() {
          console.log(that.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();

  //третий способ
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }.bind(this), 1000)
    }
  }

  person.sayHello();
```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
